<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
            }

            td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
            }

            tr:nth-child(even) {
            background-color: #dddddd;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">

                <div class="title m-b-md">
                   Tambah Lapangan
                </div>

                {{-- <div class="links" style=" padding: 10px;">
                    <a href="#" class="btn btn-xs btn-success btn-comfirm" style="color: white;">Tambah Lapangan</a>
                </div> --}}
{{--
                <table>
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Telpon</th>
                            <th>Status</th>
                            <th>Email </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ Auth::user()->first_name }} </td>
                            <td>{{ Auth::user()->last_name }} </td>
                            <td>{{ Auth::user()->email }}</td>
                            <td>{{ Auth::user()->telp }}</td>
                            <td>
                                @if(Auth::user()->status == 0)
                                <button type="" class="btn btn-xs btn-danger btn-comfirm">Inactive</button>
                                @else
                                <button type="" class="btn btn-xs btn-success btn-comfirm">Active</button>
                                @endif
                            </td>
                            <td>
                                @if(Auth::user()->is_email == 0)
                                <button type="" class="btn btn-xs btn-danger btn-comfirm">Not Verified</button>
                                @else
                                <button type="" class="btn btn-xs btn-success btn-comfirm">Verified </button>
                                @endif
                            </td>

                        </tr>
                    </tbody>
                </table> --}}
            </div>
        </div>
        <div class="links">
            <a href="{{route('core.logout')}}">Logout</a>
        </div>
    </body>
</html>

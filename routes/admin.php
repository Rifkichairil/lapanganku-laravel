<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

// Login
Route::get('/', 'CoreController@login')->middleware('guest')->name('core.login');
Route::post('/', 'CoreController@auth')->middleware('guest')->name('core.auth');

// Register
Route::get('/register', 'CoreController@register')->name('core.register');
Route::post('/register', 'CoreController@store')->name('core.store');

// Verify Status Email
Route::get('/active', 'EmailController@activated')->name('core.email.activated');



// Logout
Route::get('logout', 'CoreController@logout')->name('core.logout');

// Auth
// Auth::routes(['verify' => true]);

Route::middleware(['auth', 'verify' => true])->group(function () {
    // Route::get('dashboard', 'CoreController@dashboard')->name('core.dash');

    Route::get('/super-admin', 'SuperadminController@index')->name('core.super.index');
    Route::post('/super-admin/{id}', 'SuperadminController@updateStatus')->name('core.super.ustatus');


    Route::get('/admins', 'AdminsController@index')->name('core.admins.index');
    Route::get('/admins/create', 'AdminsController@create')->name('core.admins.create');
});

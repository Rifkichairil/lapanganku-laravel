<?php

namespace App\Http\Controllers\Admin;

use Auth;
use JsValidator;
use App\Http\Controllers\Controller;
use App\Mail\VerifyMail;
use App\Mail\Welcome;
use Illuminate\Http\Request;
use App\Repositories\CoreRepositories;
use App\Model\User;
use Illuminate\Support\Facades\Hash;
use Mail;
// use Illuminate\Support\Facades\Auth;

class CoreController extends Controller
{
    protected $coreRepository;
    public $user;

    public function __construct(CoreRepositories $coreRepository)
    {
        $this->coreRepository = $coreRepository;
    }

    public function dashboard()
    {
        return view('admin.pages.dashboard');
    }

    public function login()
    {
        $validator = JsValidator::make([
            'email'      => 'required',
            'password'   => 'required',
        ]);
        return view('admin.login', compact('validator'));
    }

    public function auth(Request $req)
    {
        $this->validate($req, [
            'email'     => 'required',
            'password'  => 'required',
        ]);

        // $credential = $req->only('email', 'password');

        // ($req->has('remember')) ? $remember = true : $remember = false;

        if (Auth::attempt(['email' => $req->email, 'password' => $req->password, 'role_id' => 99])) {
            # code...
            return redirect()->intended('admin/super-admin');
        } else if (Auth::attempt(['email' => $req->email, 'password' => $req->password, 'role_id' => 1])) {
            # code...
            return redirect()->route('core.admins.index');
        } else {
            # code...
            return redirect()->route('core.login');
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('core.login');
    }

    public function register()
    {
        return view('admin.pages.register');
    }

    public function store(Request $req)
    {
        $this->validate($req, [
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'      => 'required',
            'telp'       => 'required',
            'password'   => 'required',
            'role_id'    => 'required',
            'status'     => 'required',
            'account'    => 'required',
            'is_email'    => 'required',
        ]);

        $user = User::create([
            'first_name' => $req->first_name,
            'last_name'  => $req->last_name,
            'email'      => $req->email,
            'telp'       => $req->telp,
            'password'   => Hash::make($req->password),
            'role_id'    => $req->role_id,
            'status'     => $req->status,
            'account'    => $req->account,
            'is_email'   => $req->is_email,
        ]);
        // $this->coreRepository->save($req);

        // Use mailtrap here.
        \Mail::to($user)->send(new Welcome($user));

        return redirect()->route('core.login');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\AdminsRepositories;
use Auth;



use Illuminate\Http\Request;

class AdminsController extends Controller
{
    //
    public $user;

    protected $adminsRepository;

    public function __construct(AdminsRepositories $adminsRepository)
    {
        $this->adminsRepository = $adminsRepository;
    }

    public function index()
    {
        return view('admin.pages.normal.index');
    }

    public function create()
    {
        $user = Auth::user();

        if ($user->status == 1) {
            # code...
            return view('admin.pages.normal.create');
        }
        return redirect()->route('core.admins.index');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\User;
use App\Repositories\SuperRepositories;
use JsValidator;




class SuperadminController extends Controller
{
    protected $superRepository;
    public $user;

    public function __construct(SuperRepositories $superRepository)
    {
        $this->superRepository = $superRepository;
    }


    public function index()
    {
        $user = User::get();

        return view('admin.pages.super.index', compact('user'));
    }

    // Change Status
    public function edit($id)
    {
        $validator = JsValidator::make([
            'status' => 'required',
        ]);

        $user = $this->superRepository->first($id);

        return view('admin.pages.super.edit', compact('validator', 'user'));
    }

    public function update(Request $req, $id)
    {
        $this->validate($req, [
            'status' => 'required',
        ]);

        $user = $this->superRepository->update($req, $id);
        return redirect()->route('core.super.index')->with('msg-success', 'Berita telah diperbarui.');
    }

    public function updateStatus($id)
    {
        $this->superRepository->updateStatus($id);

        return redirect()->route('core.super.index')->with('msg-success', 'Status berita telah diperbarui.');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\Welcome;
use App\Model\User;
use Mail;



class EmailController extends Controller
{
    //
    public $user;

    public function activated(Request $req)
    {
        // dd($req);

        $user = User::where('email', $req->email)->firstOrFail();

        $user->update([
            'is_email' => '1',
        ]);

        return redirect()->route('core.login');
    }
}
